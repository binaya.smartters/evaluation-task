import React from 'react';
import {Box} from "@mui/material";
import LoginLayout from "../../src/page-components/login/LoginLayout";
import LoginForm from "../../src/page-components/login/LoginForm";

const Index = () => {

    return (
      <LoginLayout>
          <Box display="flex" alignItems={'center'} width={'100%'} justifyContent={'center'} flexDirection="column" height="100%">
              <LoginForm/>
          </Box>
      </LoginLayout>
  );
};

export default Index;
