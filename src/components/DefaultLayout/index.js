import React, {useMemo} from 'react';
import PropTypes from 'prop-types';
import DashboardLayout from "./DashboardLayout";
import MainLayout from "./MainLayout";
import {useRouter} from "next/router";

const DefaultLayout = ({children}) => {
    const [authToken, setAuthToken] = React.useState('')
    const Router = useRouter();
    const path = useMemo(()=>Router?.pathname,[Router?.pathname]);
    const notLoginPages = ['/login']
    React.useEffect(() => {
        setAuthToken(localStorage.getItem('password'))
        if(!authToken){
            Router.push('/login')
        }
    }, [])
    
    return (
        <React.Fragment>
            {
                notLoginPages.includes(path) || !authToken ? <MainLayout children={children}/> :
                    <DashboardLayout children={children}/>
            }
        </React.Fragment>
    );
};

DefaultLayout.propTypes = {
    children: PropTypes.node,
};

export default DefaultLayout;
